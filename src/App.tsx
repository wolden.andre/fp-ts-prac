import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { RouteConfig } from './app/config/routeConfig';
import InnsynRoute from './app/innsyn/InnsynRoute';

function App() {
    return (
        <div>
            <Router basename={'/'}>
                <Switch>
                    <Route exact={true} path={RouteConfig.ROOT} component={InnsynRoute} />
                </Switch>
            </Router>
        </div>
    );
}

export default App;
