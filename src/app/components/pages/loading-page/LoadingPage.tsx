import * as React from 'react';

const LoadingPage: React.FunctionComponent = (): React.ReactElement => {
    return <div>Loading ...</div>;
};

export default LoadingPage;
