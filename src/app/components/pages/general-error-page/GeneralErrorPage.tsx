import React, { useEffect } from 'react';
import VeilederLokal from './VeilederLokal';
import './generalErrorPage.less';

export interface Props {
    payload: string | undefined;
}

const GeneralErrorPage: React.FC<Props> = ({ payload }: Props): React.ReactElement => {
    useEffect(() => {
        console.info(`User on GeneralErrorPage. Error: ${JSON.stringify(payload, null, 4)}`);
    });

    return (
        <div className={'generalErrorPage'}>
            <VeilederLokal mood="uncertain" />
        </div>
    );
};

export default GeneralErrorPage;
