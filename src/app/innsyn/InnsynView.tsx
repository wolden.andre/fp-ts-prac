import * as React from 'react';
import { Essentials } from '../types/types';

interface Props {
    essentials: Essentials;
}

const InnsynView: React.FC<Props> = ({ essentials }: Props) => {
    return (
        <div>
            Innsyn logged in. Hi {essentials.person.fornavn} {essentials.person.etternavn} :)
            <div>{JSON.stringify(essentials, null, 4)}</div>
        </div>
    );
};

export default InnsynView;
