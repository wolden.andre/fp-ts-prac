import { AxiosError } from 'axios';
import HttpStatus from 'http-status-codes';
import { ResourceType } from '../types/resourceTypes';

export const isForbidden = ({ response }: AxiosError): boolean =>
    response !== undefined && response.status === HttpStatus.FORBIDDEN;

export const isUnauthorized = ({ response }: AxiosError): boolean =>
    response !== undefined && response.status === HttpStatus.UNAUTHORIZED;

export const getApiUrlByResourceType = (resourceType: ResourceType): string => {
    return `http://localhost:1234/${resourceType}`;
};
