import { StringOrNull } from '../types/types';

export const isString = (obj: unknown): obj is string => Object.prototype.toString.call(obj) === '[object String]';

export const isStringOrNull = (value: unknown): value is StringOrNull => {
    return isString(value) || value === null;
};

export function notUndefined<T>(x: T | undefined): x is T {
    return x !== undefined;
}
