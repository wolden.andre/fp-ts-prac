export enum ResourceType {
    SØKER = 'soker',
    BARN = 'barn',
    ARBEIDSGIVER = 'arbeidsgiver',
}
